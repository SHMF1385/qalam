from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse, resolve


class PersonTest(TestCase):

    def test_create_user(self):
        Person = get_user_model()
        person = Person.objects.create_user(
            username="realtestuser",
            email="realtest@example.com",
            fullname="Real Test User",
            password="testpassword1234"
        )
        self.assertEqual(person.username, "realtestuser")
        self.assertEqual(person.email, "realtest@example.com")
        self.assertEqual(person.fullname, "Real Test User")
        self.assertTrue(person.is_active)
        self.assertFalse(person.is_staff)
        self.assertFalse(person.is_superuser)

    def test_create_superuser(self):
        Person = get_user_model()
        admin_person = Person.objects.create_superuser(
            username="realtestuser",
            email="realtest@example.com",
            fullname="Real Test User",
            password="testpassword1234"
        )
        self.assertEqual(admin_person.username, "realtestuser")
        self.assertEqual(admin_person.email, "realtest@example.com")
        self.assertEqual(admin_person.fullname, "Real Test User")
        self.assertTrue(admin_person.is_active)
        self.assertTrue(admin_person.is_staff)
        self.assertTrue(admin_person.is_superuser)
