from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext as _
from django.db import models


class Person(AbstractUser):
    fullname = models.CharField(max_length=50, verbose_name=_("Fullname"))
    avatar = models.ImageField(upload_to="avatars/", blank=True, verbose_name=_("Avatar"))
    biography = models.TextField(max_length=200, blank=True, verbose_name=_("Biography"))
    followings = models.ManyToManyField('Person', related_name="Followings", blank=True, verbose_name=_("Followings"))
    followers = models.ManyToManyField('Person', related_name="Followers", blank=True, verbose_name=_("Followers"))
    joined_at = models.DateField(auto_now=True)

    def __str__(self):
        return self.fullname


    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")

