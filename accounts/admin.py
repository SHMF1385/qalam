from django.contrib import admin
from django.contrib.auth import get_user_model

Person = get_user_model()


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = ['username', 'fullname', 'email']
    fields = ('username', 'email', 'fullname', 'avatar', 'biography', 'followings', 'followers')
