# Qalam
## A blog written in django

### How to run
```
virtualenv venv
source venv/bin/activate
cp .env.sample .env
```

#### Then edit `.env` file
```
DJANGO_SECRET_KEY="Secret key"
DJANGO_DEBUG="True or False"
```

#### And `qalam/settings.py` file
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.{"Your databse backend (mysql, oracle, postgresql, sqlit)"}',
        'NAME': '{"Database name"}',
        'USER': '{"Username"}',
        'PASSWORD': '{"Password"}',
        'HOST': '{"Database Host"}',
        'PORT': '{"Database Port"}'
    }
}
```

#### And `docker-compose.yml` file
```
environment:
      - MYSQL_DATABASE=Database name
      - MYSQL_ROOT_PASSWORD=Database password

```

#### You can run server with
`docker compose up`
